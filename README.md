# icon with tooltip for vuetify2

The component is an invisible wrapper around v-icon:
 - listens to all the events emitted by v-icon
 - takes any prop v-icon can receive

## Props

 ```javascript
 props:{
   position: {
     type: String
     default: 'bottom'
   },
   noTooltip:{
     type: Boolean,
     default: false
   },
   iconClass:{
     type: String,
     default: ''
   }
 }
```
The position prop is to determine the position of the tooltip (top, bottom, left, right)

## Installation
```javascript
import { VIconToolTip } from 'vicontooltip'
```
and register as a vue component

## Usage:
```html
<VIconTooltip
  class="mr-1"
  small
  color="secondary"
  position="top"
>
  <template v-slot:description>1234</template>
  <template v-slot:icon>mdi-identifier</template>
</VIconTooltip>
```
